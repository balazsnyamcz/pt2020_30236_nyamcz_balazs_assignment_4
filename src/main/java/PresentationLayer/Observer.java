package PresentationLayer;

import BusinessLayer.Observable;

public interface Observer {
    void update();
    void setObservable(Observable observable);
}
