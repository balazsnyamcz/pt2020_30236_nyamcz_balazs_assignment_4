package PresentationLayer;

import BusinessLayer.Observable;
import javax.swing.*;
import java.awt.*;

public class ChefGraphicalUserInterface extends JFrame implements Observer {
    private String filename;
    private Observable observable;
    private JLabel label=new JLabel();
    public ChefGraphicalUserInterface(String filename){
        this.filename=filename;
        init();
    }

    private void createBackPanel(){
        Font font=new Font("Nice",Font.BOLD,14);
        JButton back = new JButton("<<BACK");
        back.setFont(font);
        back.setBackground(Color.white);
        back.setForeground(Color.BLACK);

        back.addActionListener(actionEvent -> {
            new GUI(filename);
            ChefGraphicalUserInterface.this.dispose();
        });

        JPanel panel = new JPanel();
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        panel.add(back);
        panel.setOpaque(true);

        add(panel,BorderLayout.PAGE_END);
    }

    private void createMainPanel(){
        Font font=new Font("Nice",Font.BOLD,20);

        label.setText("...waiting for job...");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setSize(400,30);
        label.setLocation(20,5);


        JPanel panel = new JPanel(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        panel.add(label);
        panel.setOpaque(true);

        add(panel);
    }

    private void init(){
        setSize(600, 400);
        setTitle("Chef");

        createBackPanel();
        createMainPanel();

        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

    }

    @Override
    public void update() {
        String msg=observable.getUpdate(this);
        if(msg==null){
            label.setText("...waiting for job...");
        }
        else
        {
            System.out.println(msg+"Chef");
            label.setText("Cooking: "+msg);
        }
    }

    @Override
    public void setObservable(Observable observable) {
        this.observable=observable;
    }
}
