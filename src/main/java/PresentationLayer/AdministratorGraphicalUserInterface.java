package PresentationLayer;

import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;
import DataLayer.RestaurantSerializator;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdministratorGraphicalUserInterface extends JFrame {
    GridBagLayout layout=new GridBagLayout();
    Restaurant restaurant=new Restaurant();
    RestaurantSerializator serializator=new RestaurantSerializator();
    JPanel pBack;
    Box pMenu;
    JPanel p1;
    JPanel p2;
    JPanel p3;
    JPanel p4;
    JPanel p5;
    JTable table=new JTable();
    String filename;

    public AdministratorGraphicalUserInterface(String filename){
        this.filename=filename;
        restaurant.setMenu(serializator.Deserialize(filename));
        init();
    }

    private JPanel createBackPanel(){
        Font font=new Font("Nice",Font.BOLD,14);
        JButton back = new JButton("<<BACK");
        back.setFont(font);
        back.setBackground(Color.white);
        back.setForeground(Color.BLACK);

        back.addActionListener(actionEvent -> {
            serializator.Serialize(filename,restaurant.getMenu());
            new GUI(filename);
            AdministratorGraphicalUserInterface.this.dispose();
        });

        JPanel panelBack = new JPanel();
        panelBack.setBackground(Color.BLACK);
        panelBack.setForeground(Color.WHITE);
        panelBack.add(back);
        panelBack.setOpaque(true);
        panelBack.setVisible(true);

        //add(panelBack,BorderLayout.PAGE_END);
        return panelBack;
    }

    private Box createMenuBox(){
        Font font=new Font("Nice",Font.BOLD,14);
        Button create = new Button("Create menu item");
        create.setFont(font);
        create.setBackground(Color.black);
        create.setForeground(Color.white);
        create.addActionListener(actionEvent -> {
            p1.setVisible(false);
            p2.setVisible(false);
            p3.setVisible(true);
            p4.setVisible(false);
            p5.setVisible(false);
        });

        Button delete = new Button("Delete menu item");
        delete.setFont(font);
        delete.setBackground(Color.black);
        delete.setForeground(Color.white);
        delete.addActionListener(actionEvent -> {
            p1.setVisible(false);
            p2.setVisible(false);
            p3.setVisible(false);
            p4.setVisible(true);
            p5.setVisible(false);
        });

        Button edit = new Button("Edit menu item");
        edit.setFont(font);
        edit.setBackground(Color.black);
        edit.setForeground(Color.white);
        edit.addActionListener(actionEvent -> {
            p1.setVisible(false);
            p2.setVisible(false);
            p3.setVisible(false);
            p4.setVisible(false);
            p5.setVisible(true);
        });

        Button view = new Button("View menu items");
        view.setFont(font);
        view.setBackground(Color.black);
        view.setForeground(Color.white);
        view.addActionListener(actionEvent -> {
            //p2=createViewItemsPanel();

            p1.setVisible(false);
            p2.setVisible(true);
            p3.setVisible(false);
            p4.setVisible(false);
            p5.setVisible(false);
        });

        Box box=Box.createVerticalBox();
        box.add(view);
        box.add(create);
        box.add(delete);
        box.add(edit);
        box.setVisible(true);

        //add(box,BorderLayout.WEST);
        return box;
    }

    private JPanel createMainPanel(){
        JPanel panel = new JPanel(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        Font font=new Font("Nice",Font.BOLD,20);
        JLabel label;

        label=new JLabel();
        label.setText("Select an operation from the left");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setLocation(150,5);
        label.setSize(label.getPreferredSize());

        panel.add(label);

        panel.setOpaque(true);
        Border border=new LineBorder(Color.WHITE,2,true);
        panel.setBorder(border);
        return panel;
    }

    private JPanel createCreateItemPanel(){
        JPanel panel = new JPanel(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        Font font=new Font("Nice",Font.BOLD,20);
        JLabel label;

        label=new JLabel();
        label.setText("Create menu item");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setLocation(150,5);
        label.setSize(label.getPreferredSize());


        JLabel jl0 = new JLabel("Type:");
        jl0.setFont(font);
        jl0.setBackground(Color.black);
        jl0.setForeground(Color.WHITE);
        jl0.setLocation(150,100);
        jl0.setSize(jl0.getPreferredSize());

        JLabel jl1 = new JLabel("Name:");
        jl1.setFont(font);
        jl1.setBackground(Color.black);
        jl1.setForeground(Color.WHITE);
        jl1.setLocation(150,150);
        jl1.setSize(jl1.getPreferredSize());


        JLabel jl2 = new JLabel("Price:");
        jl2.setFont(font);
        jl2.setBackground(Color.black);
        jl2.setForeground(Color.WHITE);
        jl2.setLocation(150,200);
        jl2.setSize(jl2.getPreferredSize());

        JTextField jtf1 = new JTextField(20);
        jtf1.setLocation(250,155);
        jtf1.setSize(jtf1.getPreferredSize());
        jtf1.setBackground(Color.WHITE);
        jtf1.setForeground(Color.BLACK);

        JTextField jtf2 = new JTextField(20);
        jtf2.setLocation(250,205);
        jtf2.setSize(jtf2.getPreferredSize());

        JLabel jl3 = new JLabel("Components:");
        jl3.setFont(font);
        jl3.setBackground(Color.black);
        jl3.setForeground(Color.WHITE);
        jl3.setLocation(150,250);
        jl3.setSize(jl3.getPreferredSize());

        JTextField jtf3 = new JTextField(20);
        jtf3.setLocation(300,255);
        jtf3.setSize(jtf3.getPreferredSize());

        JRadioButton b=new JRadioButton("Base product");
        b.setLocation(250,100);
        b.setSize(b.getPreferredSize());
        b.setBackground(Color.black);
        b.setForeground(Color.white);
        b.addActionListener(actionEvent -> {
            jl3.setVisible(false);
            jtf3.setVisible(false);
        });

        JRadioButton c=new JRadioButton("Composite product");
        c.setLocation(250,120);
        c.setSize(c.getPreferredSize());
        c.setBackground(Color.black);
        c.setForeground(Color.white);
        c.addActionListener(actionEvent -> {
            jl3.setVisible(true);
            jtf3.setVisible(true);
        });

        ButtonGroup group=new ButtonGroup();
        group.add(b);
        group.add(c);

        JButton create=new JButton("CREATE");
        create.setBackground(Color.white);
        create.setForeground(Color.black);
        create.setLocation(150,300);
        create.setSize(create.getPreferredSize());
        create.addActionListener(actionEvent -> {
            ArrayList<String> data = new ArrayList<>();

            if(b.isSelected()) {
                if(!jtf1.getText().equals("") && !jtf2.getText().equals("")) {
                    data.add(jtf1.getText().replaceAll("\\s+", ""));
                    data.add(jtf2.getText().replaceAll("\\s+", ""));

                    restaurant.createMenuItem(false, data);
                    updateTable();

                    jtf1.setText("");
                    jtf2.setText("");
                }
            }else if(c.isSelected()){
                if(!jtf1.getText().equals("") && !jtf2.getText().equals("") && !jtf3.getText().equals("")) {
                    data.add(jtf1.getText());
                    Pattern checkRegex = Pattern.compile("[a-z]+");
                    Matcher regexMatcher = checkRegex.matcher(jtf3.getText());
                    while (regexMatcher.find()) {
                        if (regexMatcher.group().length() != 0) {
                            data.add(regexMatcher.group());
                        }
                    }
                    data.add(jtf2.getText());

                    restaurant.createMenuItem(true, data);
                    updateTable();

                    jtf1.setText("");
                    jtf2.setText("");
                    jtf3.setText("");
                }
            }

        });

        panel.add(label);
        panel.add(jl0);
        panel.add(b);
        panel.add(c);
        panel.add(jl1);
        panel.add(jtf1);
        panel.add(jl2);
        panel.add(jtf2);
        panel.add(jl3);
        panel.add(jtf3);
        panel.add(create);
        panel.setOpaque(true);
        Border border=new LineBorder(Color.WHITE,2,true);
        panel.setBorder(border);
        return panel;
    }

    private JTable updateTable(){
        List<MenuItem> menu=restaurant.getMenu();
        DefaultTableModel model=new DefaultTableModel();
        model.setColumnCount(0);

        model.addColumn("Name");
        model.addColumn("Price");
        model.addColumn("Components");

        for (MenuItem item:menu){
            String[] data=new String[3];
            data[0]=item.getName();
            data[1]=""+item.computePrice();
            if(item.getClass().getTypeName().equals(CompositeProduct.class.getName())){
                data[2]=((CompositeProduct)item).getItems();
            }else {
                data[2]="";
            }
            model.addRow(data);
        }

        table.setModel(model);
        return table;
    }

    private JPanel createViewItemsPanel(){
        JPanel panel=new JPanel(null);
        Font font=new Font("Nice",Font.BOLD,20);
        JLabel label;
        label=new JLabel();
        label.setText("Menu items");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setLocation(150,5);
        label.setSize(label.getPreferredSize());

        table=updateTable();

        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setName("Menu Items");
        table.setOpaque(true);
        table.getTableHeader().setBackground(Color.black);
        table.getTableHeader().setForeground(Color.WHITE);
        table.getTableHeader().setFont(font);
        table.setRowHeight(30);

        JScrollPane scrollPane=new JScrollPane(table);
        scrollPane.setLocation(150,50);
        scrollPane.setSize(scrollPane.getPreferredSize());
        scrollPane.setBackground(Color.BLACK);
        scrollPane.getViewport().setBackground(Color.BLACK);

        panel.add(label);
        panel.add(scrollPane);
        panel.setBackground(Color.black);
        panel.setOpaque(true);
        Border border=new LineBorder(Color.WHITE,2,true);
        panel.setBorder(border);
        return panel;
    }

    private JPanel createEditItemPanel(){
        JPanel panel = new JPanel(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        Font font=new Font("Nice",Font.BOLD,20);
        JLabel label;

        label=new JLabel();
        label.setText("Edit menu item");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setLocation(150,5);
        label.setSize(label.getPreferredSize());

        JLabel jl3 = new JLabel("Item Name:");
        jl3.setFont(font);
        jl3.setBackground(Color.black);
        jl3.setForeground(Color.WHITE);
        jl3.setLocation(150,100);
        jl3.setSize(jl3.getPreferredSize());

        JTextField jtf3 = new JTextField(20);
        jtf3.setLocation(280,105);
        jtf3.setSize(jtf3.getPreferredSize());

        JLabel jl1 = new JLabel("New Name:");
        jl1.setFont(font);
        jl1.setBackground(Color.black);
        jl1.setForeground(Color.WHITE);
        jl1.setLocation(150,150);
        jl1.setSize(jl1.getPreferredSize());

        JLabel jl2 = new JLabel("New Price:");
        jl2.setFont(font);
        jl2.setBackground(Color.black);
        jl2.setForeground(Color.WHITE);
        jl2.setLocation(150,200);
        jl2.setSize(jl2.getPreferredSize());

        JTextField jtf1 = new JTextField(20);
        jtf1.setLocation(280,155);
        jtf1.setSize(jtf1.getPreferredSize());

        JTextField jtf2 = new JTextField(20);
        jtf2.setLocation(280,205);
        jtf2.setSize(jtf2.getPreferredSize());

        JButton edit=new JButton("EDIT");
        edit.setBackground(Color.white);
        edit.setForeground(Color.black);
        edit.setLocation(150,250);
        edit.setSize(edit.getPreferredSize());
        edit.addActionListener(actionEvent -> {
            ArrayList<String> data = new ArrayList<>();
            if(!jtf3.getText().equals("")){
                if(!jtf1.getText().equals("") && !jtf2.getText().equals("")){
                    data.add(jtf3.getText());
                    data.add(jtf1.getText());
                    data.add(jtf2.getText());

                    restaurant.editMenuItem(data);
                    updateTable();

                    jtf1.setText("");
                    jtf2.setText("");
                    jtf3.setText("");
                }
            }

        });

        panel.add(label);
        panel.add(jl3);
        panel.add(jtf3);
        panel.add(jl1);
        panel.add(jtf1);
        panel.add(jl2);
        panel.add(jtf2);
        panel.add(edit);

        panel.setOpaque(true);
        Border border=new LineBorder(Color.WHITE,2,true);
        panel.setBorder(border);
        return panel;
    }

    private JPanel createDeleteItemPanel(){
        JPanel panel = new JPanel(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        Font font=new Font("Nice",Font.BOLD,20);
        JLabel label;

        label=new JLabel();
        label.setText("Delete menu item");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setLocation(150,5);
        label.setSize(label.getPreferredSize());

        JLabel jl3 = new JLabel("Item Name:");
        jl3.setFont(font);
        jl3.setBackground(Color.black);
        jl3.setForeground(Color.WHITE);
        jl3.setLocation(150,100);
        jl3.setSize(jl3.getPreferredSize());

        JTextField jtf3 = new JTextField(20);
        jtf3.setLocation(280,105);
        jtf3.setSize(jtf3.getPreferredSize());

        JButton delete=new JButton("DELETE");
        delete.setBackground(Color.white);
        delete.setForeground(Color.black);
        delete.setLocation(150,150);
        delete.setSize(delete.getPreferredSize());
        delete.addActionListener(actionEvent -> {

            if(!jtf3.getText().equals("")){

                restaurant.deleteMenuItem(jtf3.getText());
                updateTable();

                jtf3.setText("");
            }
        });

        panel.add(label);
        panel.add(jl3);
        panel.add(jtf3);
        panel.add(delete);

        panel.setOpaque(true);
        Border border=new LineBorder(Color.WHITE,2,true);
        panel.setBorder(border);
        return panel;
    }

    private void init(){
        setSize(900, 600);
        setTitle("Administrator");
        //JPanel p1,p2,p3,p4,p5;
        pBack=createBackPanel();
        pMenu=createMenuBox();
        p1=createMainPanel();
        p2=createViewItemsPanel();
        p3=createCreateItemPanel();
        p4=createDeleteItemPanel();
        p5=createEditItemPanel();


        setLayout(layout);
        GridBagConstraints c=new GridBagConstraints();
        c.fill=GridBagConstraints.HORIZONTAL;
        c.weightx=0.5;
        c.anchor=GridBagConstraints.PAGE_END;
        c.gridx=0;
        c.gridy=2;
        add(pBack,c);

        c=new GridBagConstraints();
        c.fill=GridBagConstraints.VERTICAL;
        c.weighty=3;
        c.anchor=GridBagConstraints.WEST;
        c.gridx=0;
        c.gridy=0;
        add(pMenu,c);

        c=new GridBagConstraints();
        c.fill=GridBagConstraints.BOTH;
        c.gridx=0;
        c.gridy=0;
        add(p1,c);
        add(p3,c);
        add(p4,c);
        add(p5,c);

        c=new GridBagConstraints();
        c.fill=GridBagConstraints.BOTH;
        c.gridx=0;
        c.gridy=0;
        add(p2,c);

        p1.setVisible(true);
        p2.setVisible(false);
        p3.setVisible(false);
        p4.setVisible(false);
        p5.setVisible(false);

        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent){
                serializator.Serialize(filename,restaurant.getMenu());
            }
        });

        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
}

