package PresentationLayer;

import BusinessLayer.Restaurant;
import DataLayer.RestaurantSerializator;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WaiterGraphicalUserInterface extends JFrame{
    GridBagLayout layout=new GridBagLayout();
    Restaurant restaurant=new Restaurant();
    RestaurantSerializator serializator=new RestaurantSerializator();
    JPanel pBack;
    Box pMenu;
    JPanel p1;
    JPanel p3;
    JPanel p4;
    JPanel p5;
    String filename;
    public WaiterGraphicalUserInterface(String filename){
        this.filename=filename;
        restaurant.setMenu(serializator.Deserialize(filename));
        init();
    }

    private JPanel createBackPanel(){
        Font font=new Font("Nice",Font.BOLD,14);
        JButton back = new JButton("<<BACK");
        back.setFont(font);
        back.setBackground(Color.white);
        back.setForeground(Color.BLACK);

        back.addActionListener(actionEvent -> {
            serializator.Serialize(filename,restaurant.getMenu());
            new GUI(filename);
            WaiterGraphicalUserInterface.this.dispose();
        });

        JPanel panelBack = new JPanel();
        panelBack.setBackground(Color.BLACK);
        panelBack.setForeground(Color.WHITE);
        panelBack.add(back);
        panelBack.setOpaque(true);
        panelBack.setVisible(true);

        //add(panelBack,BorderLayout.PAGE_END);
        return panelBack;
    }

    private Box createMenuBox(){
        Font font=new Font("Nice",Font.BOLD,14);
        Button create = new Button("Create new order");
        create.setFont(font);
        create.setBackground(Color.black);
        create.setForeground(Color.white);
        create.addActionListener(actionEvent -> {
            p1.setVisible(false);
            p3.setVisible(true);
            p4.setVisible(false);
            p5.setVisible(false);
        });

        Button compute = new Button("Compute price for an order");
        compute.setFont(font);
        compute.setBackground(Color.black);
        compute.setForeground(Color.white);
        compute.addActionListener(actionEvent -> {
            p1.setVisible(false);
            p3.setVisible(false);
            p4.setVisible(true);
            p5.setVisible(false);
        });

        Button bill = new Button("Generate bill for an order");
        bill.setFont(font);
        bill.setBackground(Color.black);
        bill.setForeground(Color.white);
        bill.addActionListener(actionEvent -> {
            p1.setVisible(false);
            p3.setVisible(false);
            p4.setVisible(false);
            p5.setVisible(true);
        });

        Box box=Box.createVerticalBox();
        box.add(create);
        box.add(compute);
        box.add(bill);
        box.setVisible(true);

        //add(box,BorderLayout.WEST);
        return box;
    }

    private JPanel createMainPanel(){
        JPanel panel = new JPanel(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        Font font=new Font("Nice",Font.BOLD,20);
        JLabel label;

        label=new JLabel();
        label.setText("Select an operation from the left");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setLocation(215,5);
        label.setSize(label.getPreferredSize());

        panel.add(label);

        panel.setOpaque(true);
        Border border=new LineBorder(Color.WHITE,2,true);
        panel.setBorder(border);
        return panel;
    }

    private JPanel createCreateOrderPanel(){

        JPanel panel = new JPanel(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        Font font=new Font("Nice",Font.BOLD,20);
        JLabel label;

        label=new JLabel();
        label.setText("Create new order");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setLocation(215,5);
        label.setSize(label.getPreferredSize());

        JLabel jl0=new JLabel("Order id:");
        jl0.setFont(font);
        jl0.setBackground(Color.black);
        jl0.setForeground(Color.white);
        jl0.setLocation(215,100);
        jl0.setSize(jl0.getPreferredSize());

        JLabel jl1=new JLabel("Date:");
        jl1.setFont(font);
        jl1.setBackground(Color.black);
        jl1.setForeground(Color.white);
        jl1.setLocation(215,150);
        jl1.setSize(jl1.getPreferredSize());

        JLabel jl2=new JLabel("Table:");
        jl2.setFont(font);
        jl2.setBackground(Color.black);
        jl2.setForeground(Color.white);
        jl2.setLocation(215,200);
        jl2.setSize(jl2.getPreferredSize());

        JLabel jl3=new JLabel("Menu items:");
        jl3.setFont(font);
        jl3.setBackground(Color.BLACK);
        jl3.setForeground(Color.white);
        jl3.setLocation(215,250);
        jl3.setSize(jl3.getPreferredSize());

        JTextField jtf0=new JTextField(20);
        jtf0.setLocation(350,105);
        jtf0.setSize(jtf0.getPreferredSize());

        JTextField jtf1=new JTextField(20);
        jtf1.setLocation(350,155);
        jtf1.setSize(jtf1.getPreferredSize());

        JTextField jtf2=new JTextField(20);
        jtf2.setLocation(350,205);
        jtf2.setSize(jtf2.getPreferredSize());

        JTextField jtf3=new JTextField(20);
        jtf3.setLocation(350,255);
        jtf3.setSize(jtf3.getPreferredSize());

        JButton create=new JButton("CREATE");
        create.setBackground(Color.white);
        create.setForeground(Color.black);
        create.setLocation(215,300);
        create.setSize(create.getPreferredSize());
        create.addActionListener(actionEvent -> {
            ArrayList<String> data=new ArrayList<>();

            if(!jtf0.getText().equals("") && !jtf1.getText().equals("") && !jtf2.getText().equals("") && !jtf3.getText().equals("")){
                data.add(jtf0.getText());
                data.add(jtf1.getText());
                data.add(jtf2.getText());
                Pattern checkRegex=Pattern.compile("[a-z]+");
                Matcher regexMatcher=checkRegex.matcher(jtf3.getText());
                while (regexMatcher.find()){
                    if(regexMatcher.group().length()!=0){
                        data.add(regexMatcher.group());
                    }
                }
                restaurant.createOrder(data);

                jtf0.setText("");
                jtf1.setText("");
                jtf2.setText("");
                jtf3.setText("");
            }
        });

        panel.add(label);
        panel.add(jl0);
        panel.add(jtf0);
        panel.add(jl1);
        panel.add(jtf1);
        panel.add(jl2);
        panel.add(jtf2);
        panel.add(jl3);
        panel.add(jtf3);
        panel.add(create);
        panel.setOpaque(true);
        Border border=new LineBorder(Color.WHITE,2,true);
        panel.setBorder(border);
        return panel;
    }

    private JPanel createComputePricePanel(){
        JPanel panel = new JPanel(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        Font font=new Font("Nice",Font.BOLD,20);
        JLabel label;

        label=new JLabel();
        label.setText("Compute price for an order");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setLocation(215,5);
        label.setSize(label.getPreferredSize());

        JLabel jl0=new JLabel("Order id:");
        jl0.setFont(font);
        jl0.setBackground(Color.black);
        jl0.setForeground(Color.white);
        jl0.setLocation(215,100);
        jl0.setSize(jl0.getPreferredSize());

        JLabel jl1=new JLabel("Date:");
        jl1.setFont(font);
        jl1.setBackground(Color.black);
        jl1.setForeground(Color.white);
        jl1.setLocation(215,150);
        jl1.setSize(jl1.getPreferredSize());

        JLabel jl2=new JLabel("Table:");
        jl2.setFont(font);
        jl2.setBackground(Color.black);
        jl2.setForeground(Color.white);
        jl2.setLocation(215,200);
        jl2.setSize(jl2.getPreferredSize());

        JTextField jtf0=new JTextField(20);
        jtf0.setLocation(350,105);
        jtf0.setSize(jtf0.getPreferredSize());

        JTextField jtf1=new JTextField(20);
        jtf1.setLocation(350,155);
        jtf1.setSize(jtf1.getPreferredSize());

        JTextField jtf2=new JTextField(20);
        jtf2.setLocation(350,205);
        jtf2.setSize(jtf2.getPreferredSize());

        JLabel jl3=new JLabel("");
        jl3.setFont(font);
        jl3.setBackground(Color.black);
        jl3.setForeground(Color.white);
        jl3.setLocation(215,300);
        jl3.setSize(new Dimension(600,30));

        JButton price=new JButton("COMPUTE PRICE");
        price.setBackground(Color.white);
        price.setForeground(Color.black);
        price.setLocation(215,250);
        price.setSize(price.getPreferredSize());
        price.addActionListener(actionEvent -> {
            ArrayList<String> data=new ArrayList<>();

            if(!jtf0.getText().equals("") && !jtf1.getText().equals("") && !jtf2.getText().equals("")) {
                data.add(jtf0.getText());
                data.add(jtf1.getText());
                data.add(jtf2.getText());
                int sum=restaurant.computeOrderPrice(data);
                jl3.setText("The computed price is: "+sum);
            }
            jtf0.setText("");
            jtf1.setText("");
            jtf2.setText("");
        });

        panel.add(label);
        panel.add(jl0);
        panel.add(jl1);
        panel.add(jl2);
        panel.add(jtf0);
        panel.add(jtf1);
        panel.add(jtf2);
        panel.add(price);
        panel.add(jl3);

        panel.setOpaque(true);
        Border border=new LineBorder(Color.WHITE,2,true);
        panel.setBorder(border);
        return panel;
    }

    private JPanel createGenerateBillPanel(){
        JPanel panel = new JPanel(null);
        panel.setBackground(Color.BLACK);
        panel.setForeground(Color.WHITE);
        Font font=new Font("Nice",Font.BOLD,20);
        JLabel label;

        label=new JLabel();
        label.setText("Generate bill for an order");
        label.setFont(font);
        label.setBackground(Color.black);
        label.setForeground(Color.WHITE);
        label.setLocation(215,5);
        label.setSize(label.getPreferredSize());

        JLabel jl0=new JLabel("Order id:");
        jl0.setFont(font);
        jl0.setBackground(Color.black);
        jl0.setForeground(Color.white);
        jl0.setLocation(215,100);
        jl0.setSize(jl0.getPreferredSize());

        JLabel jl1=new JLabel("Date:");
        jl1.setFont(font);
        jl1.setBackground(Color.black);
        jl1.setForeground(Color.white);
        jl1.setLocation(215,150);
        jl1.setSize(jl1.getPreferredSize());

        JLabel jl2=new JLabel("Table:");
        jl2.setFont(font);
        jl2.setBackground(Color.black);
        jl2.setForeground(Color.white);
        jl2.setLocation(215,200);
        jl2.setSize(jl2.getPreferredSize());

        JTextField jtf0=new JTextField(20);
        jtf0.setLocation(350,105);
        jtf0.setSize(jtf0.getPreferredSize());

        JTextField jtf1=new JTextField(20);
        jtf1.setLocation(350,155);
        jtf1.setSize(jtf1.getPreferredSize());

        JTextField jtf2=new JTextField(20);
        jtf2.setLocation(350,205);
        jtf2.setSize(jtf2.getPreferredSize());

        JLabel jl3=new JLabel("");
        jl3.setFont(font);
        jl3.setBackground(Color.black);
        jl3.setForeground(Color.white);
        jl3.setLocation(215,300);
        jl3.setSize(new Dimension(600,30));

        JButton bill=new JButton("GENERATE");
        bill.setBackground(Color.white);
        bill.setForeground(Color.black);
        bill.setLocation(215,250);
        bill.setSize(bill.getPreferredSize());
        bill.addActionListener(actionEvent -> {
            ArrayList<String> data=new ArrayList<>();

            if(!jtf0.getText().equals("") && !jtf1.getText().equals("") && !jtf2.getText().equals("")) {
                data.add(jtf0.getText());
                data.add(jtf1.getText());
                data.add(jtf2.getText());
                String name=restaurant.generateBill(data);
                jl3.setText("The generated bill name is: "+name);
            }
            jtf0.setText("");
            jtf1.setText("");
            jtf2.setText("");
        });

        panel.add(label);
        panel.add(jl0);
        panel.add(jl1);
        panel.add(jl2);
        panel.add(jtf0);
        panel.add(jtf1);
        panel.add(jtf2);
        panel.add(bill);
        panel.add(jl3);

        panel.setOpaque(true);
        Border border=new LineBorder(Color.WHITE,2,true);
        panel.setBorder(border);
        return panel;
    }

    private void init(){
        setSize(900, 600);
        setTitle("Waiter");

        pBack=createBackPanel();
        pMenu=createMenuBox();
        p1=createMainPanel();
        p3=createCreateOrderPanel();
        p4=createComputePricePanel();
        p5=createGenerateBillPanel();

        setLayout(layout);
        GridBagConstraints c=new GridBagConstraints();
        c.fill=GridBagConstraints.HORIZONTAL;
        c.weightx=0.5;
        c.anchor=GridBagConstraints.PAGE_END;
        c.gridx=0;
        c.gridy=2;
        add(pBack,c);

        c=new GridBagConstraints();
        c.fill=GridBagConstraints.VERTICAL;
        c.weighty=3;
        c.anchor=GridBagConstraints.WEST;
        c.gridx=0;
        c.gridy=0;
        add(pMenu,c);

        c=new GridBagConstraints();
        c.fill=GridBagConstraints.BOTH;
        c.gridx=0;
        c.gridy=0;
        add(p1,c);
        add(p3,c);
        add(p4,c);
        add(p5,c);

        p1.setVisible(true);
        p3.setVisible(false);
        p4.setVisible(false);
        p5.setVisible(false);

        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent){
                serializator.Serialize(filename,restaurant.getMenu());
            }
        });

        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

    }
}
