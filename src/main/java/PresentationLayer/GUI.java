package PresentationLayer;

//import BusinessLayer.Observable;
//import BusinessLayer.Restaurant;
import javax.swing.*;
import java.awt.*;
import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;

public class GUI extends JFrame {
    String filename;
    //WaiterGraphicalUserInterface wGUI;
   // ChefGraphicalUserInterface cGUI;

    public GUI(String filename) {
        this.filename=filename;
        init();
    }

    private void init(){
        setSize(600, 400);
        setTitle("Restaurant!");
        Font font=new Font("Nice",Font.BOLD,20);
        Button admin = new Button("Administrator");
        admin.setForeground(WHITE);
        admin.setBackground(BLACK);
        admin.setFont(font);
        admin.addActionListener(actionEvent -> {
            new AdministratorGraphicalUserInterface(filename);
            GUI.this.dispose();
        });

        //wGUI=new WaiterGraphicalUserInterface(filename);
        //wGUI.setVisible(false);
        Button waiter = new Button("Waiter");
        waiter.setForeground(WHITE);
        waiter.setBackground(BLACK);
        waiter.setFont(font);
        waiter.addActionListener(actionEvent -> {
            new WaiterGraphicalUserInterface(filename);
            //wGUI.setVisible(true);
            GUI.this.dispose();
        });

        //cGUI=new ChefGraphicalUserInterface(filename);
        //cGUI.setVisible(false);
        Button chef = new Button("Chef");
        chef.setForeground(WHITE);
        chef.setBackground(BLACK);
        chef.setFont(font);
        chef.addActionListener(actionEvent -> {
            new ChefGraphicalUserInterface(filename);
            //cGUI.setVisible(true);
            GUI.this.dispose();
        });


        //wGUI.restaurant.add(cGUI);
        //cGUI.setObservable(wGUI.restaurant);

        Box box = Box.createVerticalBox();
        box.add(admin);
        box.add(waiter);
        box.add(chef);

        add(box,BorderLayout.CENTER);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

}