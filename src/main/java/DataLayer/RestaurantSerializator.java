package DataLayer;

import BusinessLayer.MenuItem;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class RestaurantSerializator {
    public void Serialize(String filename, List<MenuItem> menu){
        try
        {
            //Salveaza un obiect intr-un filsier
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);
            // serializarea obiectului
            out.writeInt(menu.size());
            for (MenuItem item:menu) {
                out.writeObject(item);
            }
            out.close();
            file.close();
            System.out.println("Object has been serialized");
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public List<MenuItem> Deserialize(String filename){
        List<MenuItem> menu=new ArrayList<>();
        try {
            FileInputStream file=new FileInputStream(filename);
            ObjectInputStream in=new ObjectInputStream(file);
            int size=in.readInt();
            MenuItem item;
            for (int i=0;i<size;i++){
                item=(MenuItem) in.readObject();
                menu.add(item);
            }
            in.close();
            file.close();
            System.out.println("Object has been deserialized ");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return menu;
    }
}
