package DataLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collection;

public class FileWriter {
    public String bill(Order order, Collection<MenuItem> items, int price){
        String filename = "Order_";
        filename += order.getOrderID();
        filename+="_";
        filename+=order.getDate();
        filename+="_";
        filename+=order.getTable();
        filename += ".txt";
        try (PrintWriter w = new PrintWriter(filename)) {
            w.println("Comanda nr. " + order.getOrderID() + " in luna " + order.getDate() + " la masa " + order.getTable() + ":");
            for (MenuItem item : items) {
                w.println(item);
            }
            w.println("Pret total: " + price);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }
}
