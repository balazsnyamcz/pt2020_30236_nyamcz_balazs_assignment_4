import BusinessLayer.*;
import DataLayer.RestaurantSerializator;
import PresentationLayer.GUI;

import java.util.*;

public class Main {

        private static void initialValue(Restaurant restaurant){
                ArrayList<String> data = new ArrayList<>();
                data.add("a");
                data.add("1");
                restaurant.createMenuItem(false, data);

                data =new ArrayList<>();
                data.add("b");
                data.add("2");
                restaurant.createMenuItem(false, data);

                data =new ArrayList<>();
                data.add("c");
                data.add("a");
                data.add("b");
                data.add("4");
                restaurant.createMenuItem(true, data);

                data =new ArrayList<>();
                data.add("d");
                data.add("5");
                restaurant.createMenuItem(false, data);

                data =new ArrayList<>();
                data.add("e");
                data.add("d");
                data.add("c");
                data.add("b");
                data.add("13");
                restaurant.createMenuItem(true, data);
        }

        public static void main(String[] args){
        Restaurant restaurant=new Restaurant();
        initialValue(restaurant);

        RestaurantSerializator serializator=new RestaurantSerializator();
        serializator.Serialize(args[0],restaurant.getMenu());

        new GUI(args[0]);


    }
}
