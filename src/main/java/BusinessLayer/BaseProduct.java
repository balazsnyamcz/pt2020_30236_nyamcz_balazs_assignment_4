package BusinessLayer;

public class BaseProduct extends MenuItem {
    private String name;
    private int price;

    public BaseProduct(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public int computePrice(){
        return this.price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return name +": " + price;
    }
}
