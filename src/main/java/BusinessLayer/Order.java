package BusinessLayer;

public class Order {
    private int orderID;
    private String date;
    private int table;

    public Order(int orderID, String date, int table) {
        this.orderID = orderID;
        this.date = date;
        this.table = table;
    }

    public int getOrderID() {
        return orderID;
    }

    public String getDate() {
        return date;
    }

    public int getTable() {
        return table;
    }

    public int hashCode(){
        int hash=0;
        hash+=date.hashCode();
        hash+=table;
        hash+=orderID;
        return hash;
    }

    public String toString(){
        return "("+orderID+","+date+","+table+")";
    }
}
