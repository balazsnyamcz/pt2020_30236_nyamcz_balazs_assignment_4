package BusinessLayer;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem {
    private String name;
    private List<MenuItem> itemList;
    private int price;

    public CompositeProduct(String name, int price) {
        this.name = name;
        this.itemList = new ArrayList<>();
        this.price=price;
    }

    public CompositeProduct(){
        this.itemList=new ArrayList<>();
    }

    public int computePrice(){
        /*int price=0;
        for (MenuItem menuItem: itemList) {
            price+=menuItem.computePrice();
        }
        */
        return this.price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<MenuItem> getItemList() {
        return itemList;
    }

    public String getItems(){
        StringBuilder string= new StringBuilder();
        for(MenuItem item:itemList){
            string.append(item.getName());
            string.append(", ");
        }
        string.delete(string.lastIndexOf(","),string.length());
        return string.toString();
    }

    public void addItem(MenuItem menuItem){
        this.itemList.add(menuItem);
    }

    @Override
    public String toString() {
        StringBuilder cp= new StringBuilder();
        cp.append(this.name);
        cp.append("[");
        for (MenuItem menuItem:itemList) {
            if(menuItem.getClass().equals(BaseProduct.class)) {
                cp.append(menuItem.getName()).append(", ");
            }else{
                cp.append(menuItem.toString());
                cp.delete(cp.lastIndexOf(":"),cp.length());
                cp.append(", ");
            }
        }
        cp.delete(cp.lastIndexOf(","),cp.length());
        cp.append("]");
        cp.append(": ").append(computePrice());


        return cp.toString();
    }
}
