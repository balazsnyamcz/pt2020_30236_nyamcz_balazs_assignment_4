package BusinessLayer;

import DataLayer.FileWriter;
import PresentationLayer.Observer;

import java.io.Serializable;
import java.util.*;

public class Restaurant implements Observable,IRestaurantProcessing, Serializable {
    private Map<Order, Collection<MenuItem>> orders;
    private List<MenuItem> menu;
    private List<Observer> observers;
    private String message;
    private boolean changed;
    private final Object MUTEX=new Object();

    public Restaurant(){
        orders= new HashMap<>();
        menu=new ArrayList<>();
        observers=new ArrayList<>();
    }

    private void createBaseProduct(ArrayList<String> data){
        MenuItem m;
        boolean exist=false;
        if(data.size()==2){
            for(MenuItem item:menu){
                if(data.get(0).equals(item.getName())){
                    exist=true;
                    break;
                }
            }
            if(!exist) {
                m = new BaseProduct(data.get(0), Integer.parseInt(data.get(1)));
                menu.add(m);
            }
        }else{
            System.out.println("You have to fill Name and Price fields too");
        }
    }

    private void createCompositeProduct(ArrayList<String> data){
        CompositeProduct m;
        boolean exist=false;
        for(MenuItem item:menu)
            if(data.get(0).equals(item.getName())){
                exist=true; break;
            }
        if(!exist) {
            m = new CompositeProduct();
            m.setName(data.get(0)); //Setez numele produsului
            m.setPrice(Integer.parseInt(data.get(data.size() - 1))); //Setez pretul produsului
            boolean flag = false;
            for (int i = 1; i < data.size() - 1; i++) { //Pentru fiecare nume de produs primit ca parametru
                String name = data.get(i);
                for (MenuItem menuItem : menu)  //Verific daca exista produsul in menu
                    if (menuItem.getName().equals(name)) {//Daca exista, il adaug  in lista de ingrediente
                        m.addItem(menuItem);
                        flag = true;
                        break;
                    }
                if (!flag) { //Daca nu am gasit in menu, afisez o eroare.
                    System.out.println("You have to create the " + name + " product first");
                    break;
                }
            }
            if (flag) {
                menu.add(m);
            }
        }
    }

    @Override
    public void createMenuItem(boolean item, ArrayList<String> data) {
        if(!item) createBaseProduct(data);
        else createCompositeProduct(data);
        showMenuItems();
    }

    private void deleteAux(List<MenuItem> menu, MenuItem item,List<MenuItem> toDelete){
        for(MenuItem i:menu){
            if(i.equals(item)){
                if(!toDelete.contains(i)){
                    toDelete.add(i);
                    deleteAux(menu,i,toDelete);
                }
            }
            if (i.getClass().getTypeName().equals(CompositeProduct.class.getName())) {
                if (((CompositeProduct) i).getItemList().contains(item)) {
                    toDelete.add(i);
                    deleteAux(menu,i,toDelete);
                }
            }
        }
    }

    @Override
    public void deleteMenuItem(String name) {
        List<MenuItem> toDelete=new ArrayList<>();

        for (MenuItem item:menu) {
            if(item.getName().equals(name)){
                toDelete.add(item);
                deleteAux(menu,item,toDelete);
            }
        }

        for (MenuItem i:toDelete) {
            menu.remove(i);
        }
        showMenuItems();
    }

    @Override
    public void editMenuItem(ArrayList<String> data) {
        if(data.size()==3){
            for (MenuItem item:menu) {
                if(item.getName().equals(data.get(0))){
                    item.setName(data.get(1));
                    item.setPrice(Integer.parseInt(data.get(2)));
                }
            }
        }else{
            System.out.println("You have to specify the name of the product, the NewName and the NewPrice fields too");
        }
        showMenuItems();
    }

    @Override
    public void createOrder(ArrayList<String> data) {
        if(data.size()>=4){
            boolean found=false;
            Order order=new Order(Integer.parseInt(data.get(0)),data.get(1),Integer.parseInt(data.get(2)));
            Collection<MenuItem> menuItemCollection = new ArrayList<>();
            for (int i=3;i<data.size();i++){
                found=false;
                for (MenuItem item:menu){
                    if(item.getName().equals(data.get(i))){
                        menuItemCollection.add(item);
                        found=true;
                        break;
                    }
                }
                if(!found){
                    System.out.println("Nu exista menu item-ul "+ data.get(i)+" in meniul restaurantului");
                    break;
                }
            }
            if(found){
                orders.put(order, menuItemCollection);
                StringBuilder string= new StringBuilder();
                for (MenuItem item:menuItemCollection)
                    string.append(item.getName()).append(" ");
                System.out.println(order + " " +string.toString());
                this.message=string.toString();
                this.changed=true;
                notifyAllObservers();
            }
        }
    }

    @Override
    public int computeOrderPrice(ArrayList<String> data) {
        int sum = 0;
        if(data.size()==3) {
            for (Map.Entry<Order, Collection<MenuItem>> entry : orders.entrySet()) {
                if (entry.getKey().getOrderID() == Integer.parseInt(data.get(0)) &&
                        entry.getKey().getDate().equals(data.get(1)) &&
                        entry.getKey().getTable()==Integer.parseInt(data.get(2))) {
                    for (MenuItem item : entry.getValue()) {
                        sum += item.computePrice();
                    }
                    break;
                }
            }
        }
        if(sum==0) {
            System.out.println("Nu exista orderul respectiv");
            return 0;
        }
        return sum;
    }

    @Override
    public String generateBill(ArrayList<String> data) {
        FileWriter writer=new FileWriter();
        if(data.size()==3){
            for (Map.Entry<Order, Collection<MenuItem>> entry : orders.entrySet()) {
                if (entry.getKey().getOrderID() == Integer.parseInt(data.get(0)) &&
                        entry.getKey().getDate().equals(data.get(1)) &&
                        entry.getKey().getTable()==Integer.parseInt(data.get(2))) {
                    return writer.bill(entry.getKey(),entry.getValue(),computeOrderPrice(data));
                }
            }
        }
        return "";
    }

    public List<MenuItem> getMenu() {
        return menu;
    }

    public void setMenu(List<MenuItem> menu) {
        this.menu = menu;
    }

    public void showMenuItems(){
        for (MenuItem item:menu) {
            System.out.println(item.toString());
        }
        System.out.println();
    }

    @Override
    public void add(Observer observer) {
        if(observer==null) throw new NullPointerException("Null Observer");
        synchronized (MUTEX){
            if(!observers.contains(observer)) observers.add(observer);
        }
    }

    @Override
    public void notifyAllObservers() {
        List<Observer> observersLocal;
        synchronized (MUTEX){
            if (!changed)
                return;
            observersLocal=new ArrayList<>(this.observers);
            this.changed=false;
        }
        for(Observer observer:observersLocal){
            observer.update();
        }
    }

    @Override
    public String getUpdate(Observer observer) {
        return this.message;
    }
}
