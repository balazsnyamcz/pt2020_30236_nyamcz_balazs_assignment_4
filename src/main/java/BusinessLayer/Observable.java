package BusinessLayer;

import PresentationLayer.Observer;

public interface Observable {
    void add(Observer observer);
    void notifyAllObservers();
    String getUpdate(Observer observer);
}
