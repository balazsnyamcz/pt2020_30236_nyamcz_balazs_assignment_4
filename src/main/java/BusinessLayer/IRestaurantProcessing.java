package BusinessLayer;

import java.util.ArrayList;

public interface IRestaurantProcessing {
    void createMenuItem(boolean item, ArrayList<String> data);
    void deleteMenuItem(String name);
    void editMenuItem(ArrayList<String> data);
    void createOrder(ArrayList<String> data);
    int computeOrderPrice(ArrayList<String> data);
    String generateBill(ArrayList<String> data);
}
