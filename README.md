Consider implementing a restaurant management system. The system should have three types of
users: administrator, waiter and chef. The administrator can add, delete and modify existing
products from the menu. The waiter can create a new order for a table, add elements from the
menu, and compute the bill for an order. The chef is notified each time it must cook food that is
ordered through a waiter.

Consider the system of classes in the diagram below.
To simplify the application, you may assume that the system is used by only one administrator,
one waiter and one chef, and there is no need of a login process.
Solve the following:

1) Define the interface IRestaurantProcessing containing the main operations that can be
executed by the waiter/administrator, as follows:
• Administrator: create new menu item, delete menu item, edit menu item
• Waiter: create new order; compute price for an order; generate bill in .txt
format.

2) Define and implement the classes from the class diagram shown above:
• Use the Composite Design Pattern for defining the classes MenuItem,
BaseProduct and CompositeProduct 
• Use the Observer Design Pattern to notify the chef each time a new order
containing a composite product is added.

3) Implement the class Restaurant using a predefined JCF collection that is based on a
hashtable data structure. The hashtable key will be generated based on the class Order,
which can have associated several MenuItems. Use JTable to display Restaurant related
information.
• Define a structure of type Map<Order, Collection<MenuItem>> for storing the
order related information in the Restaurant class. The key of the Map will be
formed of objects of type Order, for which the hashCode() method will be
overwritten to compute the hash value within the Map from the attributes of the
Order (OrderID, date, etc.).
• Define an appropriate collection consisting of MenuItem objects to store the
menu of the restaurant.
• Define a method of type “well formed” for the class Restaurant.
• Implement the class Restaurant using Design by Contract method (involving
pre, post conditions, invariants, and assertions).

4) The menu items for populating the Restaurant object will be loaded/saved from/to a file
using Serialization.


Minimum to pass
• Object-oriented programming design
• Classes with maximum 300 lines
• Methods with maximum 30 lines
• Java naming conventions
• Basic documentation
• Implement the class diagram from the homework specification. Choose
wisely the appropriate data structures for saving the Orders and the
MenuItems
• Graphical interface:
o Window for Administrator operations: add new MenuItem, edit
MenuItems, delete MenuItems, view all MenuItems in a table
(JTable)
o Window for Waiter operations: add new Order, view all Orders in a
table (JTable), compute bill for an Order
• jar file - the application should permit to be run with the following command:
java -jar PT2020_Group_LastName_FirstName_Assignment_4.jar

Quality of the Documentation 

Use Composite Design Pattern for modelling the classes MenuItem, BaseProduct,
CompositeProduct.

Create bill in .txt format. 

Design by contract: preconditions and postconditions in the IRestaurantProcessing
interface. Implement them in the Restaurant class using the assert instruction. Define
an invariant for the class Restaurant.

Window for Chef user: use Observer Design Pattern to notify each time a new Order
is added


Save the information from the Restaurant class in a file (i.e. restaurant.ser) using
serialization. Load the information when the application starts. Consequently, the
application should permit to be run with the following command:
java -jar PT2020_Group_LastName_FirstName_Assignment_4.jar restaurant.ser
